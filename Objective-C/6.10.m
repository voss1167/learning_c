// Program to generate a table of prime numbers

#import <Foundation/Foundation.h>

#define TableValue 13195 // Max table value

int main (int argc, char * argv[])
{
  @autoreleasepool {
    int p, d, isPrime;

    for ( p = 2; p <= TableValue; ++p) {
      isPrime = 1;

      for ( d = 2; d < p; ++d)
        if ( p % d == 0 )
          isPrime = 0;

      if ( isPrime != 0 )
        NSLog (@"%i", p);
    }
  }

  return 0;
}
