// Program to evaluate simple expression of the form
//       Value operator value

#import <Foundation/Foundation.h>

// Insert interface and implementation sections for
// Calculator class here

int main (int argc, char * argv[])
{
  @autoreleasepool {
    double value1, value2;
    char operator;
    Calculator *deskCalc = [[Calculator alloc] init];

    NSLog (@"Type in your expression.");
    scanf ("%lf %c %lf", &value1, &operator, &value2);

    [deskCalc setAccumulator: value1];

    switch (operator
