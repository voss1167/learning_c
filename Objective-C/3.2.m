// Program to work with fractions – class version

#import <Foundation/Foundation.h>

//---- @interface section ----

@interface Fraction: NSObject

-(void)   print;
-(void)   setNumerator: (int) n;
-(void)   setDenominator: (int) d;

@end

//---- @implementation section ----

@implementation Fraction
{
   int  numerator;
   int  denominator;
}
-(void) print
{
   NSLog (@"%i/%i", numerator, denominator);
}

-(void) setNumerator: (int) n
{
   numerator = n;
}

-(void) setDenominator: (int) d
{
   denominator = d;
}

@end

//---- program section ----


int main (int argc, char * argv[])
{
   @autoreleasepool {
     Fraction  *myFraction1 = [[Fraction alloc]init];
     Fraction  *myFraction2 = [[Fraction alloc]init];

      // Set fraction1 to 1/3

      [myFraction1 setNumerator: 1];
      [myFraction1 setDenominator: 3];

      // Set fraction2 to 2/5

      [myFraction2 setNumerator: 2];
      [myFraction2 setDenominator: 5];

      // Display the fraction using the print method

      NSLog (@"The value of myFraction1 is:");
      [myFraction1 print];

      NSLog (@"The value of myFraction2 is:");
      [myFraction2 print];
   }
   return 0;
}
