#include <iostream>

using namespace std;

int main()
{
  bool coinFlippedHeads = true;
  long int largeNumber = 2.5e12;

  cout << "coinFlippedHeads = " << coinFlippedHeads;
  cout << " , sizeof(coinFlippedHeads) = " << sizeof(coinFlippedHeads) << endl;
  cout << "largeNumber = " << largeNumber;
  cout << " , sizeof(largeNumber) = " << sizeof(largeNumber) << endl;

  return 0;
}
