#include <iostream>

int main()
{
  using namespace std;
  cout << "Computing the size of some C++ inbuilt variable types" << endl;

  cout << "Size of bool: " << sizeof(bool) << endl;
  cout << "Size of char: " << sizeof(char) << endl;

  cout << "The output changes with compiler, hardware and OS" << endl;

  return 0;
}
