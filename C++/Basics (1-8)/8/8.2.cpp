#include <iostream>
using namespace std;

int main()
{
  int age = 30;
  int* pointsToInt = &age; // pointer initialized to &age

  // Displaying th evalue of pointer
  cout << "Interger age is at: " << pointsToInt << endl;
  cout << "Interger age is at: " << hex << pointsToInt << endl;
  cout << "Interger age is at: " << &age << endl;
  cout << "Age is: " << dec << *pointsToInt << endl;

  return 0;
}
