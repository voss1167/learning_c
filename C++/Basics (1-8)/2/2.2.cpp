// pre-processor directive
#include <iostream>

// Start of your program
int main()
{
  // Tell the comiler that namespace to seach in
  using namespace std;

  // write to the screen using std::cout
  cout << "Hello World" << endl;

  return 0;
}
