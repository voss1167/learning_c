#include <iostream>
using namespace std;

// Function declaration and definition
int DemoConsoleOutput()
{
  cout << "Hello World" << endl;

  return 0;
}

int main()
{
  // Function call with return used to exit
  return DemoConsoleOutput();
}
